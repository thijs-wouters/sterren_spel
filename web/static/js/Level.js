import {createStar} from './star'

var platforms;
var player;
var cursors;
var stars;
var score = 0;
var scoreText;

export class Level extends Phaser.State {
  init(...args) {
    [this.levelNumber] = args;
  }

  preload() {
    this.load.image('sky', 'assets/sky.png');
    this.load.image('ground', 'assets/platform.png');
    this.load.image('star', 'assets/star.png');
    this.load.spritesheet('dude', 'assets/dude.png', 32, 48);
    var client = new XMLHttpRequest();
    client.open('GET', '/assets/levels/level' + this.levelNumber + '.txt', false);
    client.send();
    console.log(client.status);
    if (client.status == 200) {
      this.level = this.parse(client.responseText);
    } else {
      this.levelNumber = 1;
      var client = new XMLHttpRequest();
      client.open('GET', '/assets/levels/level' + this.levelNumber + '.txt', false);
      client.send();
      this.level = this.parse(client.responseText);
    }
  }

  parse(level) {
    return level.trim().split(/\r?\n/).map(function(line){
      return line.split('');
    });
  }

  create() {
    this.physics.startSystem(Phaser.Physics.ARCADE);

    this.add.sprite(0, 0, 'sky');

    platforms = this.add.group();
    platforms.enableBody = true;

    this.level.forEach(function(row, rowNumber) {
      row.forEach(function(cell, cellNumber) {
        if (cell == '1') {
          const testing = platforms.create(cellNumber * 32, rowNumber * 32, 'ground');
          testing.scale.setTo(0.08, 1);
          testing.body.immovable = true;
        }
      });
    });

    player = this.add.sprite(32, this.world.height - 150, 'dude');
    this.physics.arcade.enable(player);

    player.body.bounce.y = 0.2;
    player.body.gravity.y = 300;
    player.body.collideWorldBounds = true;

    player.animations.add('left', [0, 1, 2, 3], 10, true);
    player.animations.add('right', [5, 6, 7, 8], 10, true);

    cursors = this.input.keyboard.createCursorKeys();

    stars = this.add.group();
    stars.enableBody = true;

    for (var i = 0;i < 12; i++) {
      createStar(stars, i*70, 0);
    }

    scoreText = this.add.text(16, 16, 'Score: 0', { fontSize: '32px', fill: '#000'});
  }

  update() {
    this.physics.arcade.collide(player, platforms);
    this.physics.arcade.collide(platforms, stars);
    this.physics.arcade.overlap(player, stars, this.collectStar, null, this);

    if(cursors.left.isDown) {
      player.body.velocity.x = -150;
      player.animations.play('left');
    } else if(cursors.right.isDown) {
      player.body.velocity.x = 150;
      player.animations.play('right');
    } else {
      player.animations.stop();
      player.body.velocity.x = 0;
      player.frame = 4;
    }

    if (cursors.up.isDown && player.body.touching.down) {
      player.body.velocity.y = -350;
    }
  }

  collectStar(player, star) {
    star.kill();
    score += 10;
    scoreText.text = 'Score: ' + score;
    if (this.noStarsLeft()) {
      this.game.state.start('level', false, false, this.levelNumber + 1);
    }
  }

  noStarsLeft() {
    return stars.children.map(function(star) {
      return !star.alive;
    }).reduce(function(acc, starAlive) {
      return acc && starAlive;
    });
  }
}
