export function createStar(stars, x, y) {
    var star = stars.create(x, y, 'star');

    star.body.gravity.y = 6;
    star.body.bounce.y = 0.7 + Math.random() * 0.2;
}
